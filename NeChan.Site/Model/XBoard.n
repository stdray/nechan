﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Linq;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using LinqToDB;
using NBoxx.Core.Sugar;
using NeChan.Model;
using NeChan.Parser;

public class XBoard {
    public mutable static RepCtor : void -> IBoardRepository;
    static syncObject        : Object = Object();
    static mutable _boards   : ConcurrentDictionary[string, XBoard];
    protected static Boards  : ConcurrentDictionary[string, XBoard] {
        get {
            lockwhen(syncObject, _boards == null)
                _boards = LoadBoards();
            _boards
        }
    }
    public static GetBoards() : Seq[XBoard] {
        Boards.Values.OrderBy(_.Path);
    }
    public Info      : Board;
    mutable _number  : Int64;
    public PagesCache : ConcurrentDictionary[string, FileCacheItem] = ConcurrentDictionary();
    mutable _threads : ConcurrentDictionary[String, XThread];
    public  Threads : ConcurrentDictionary[String, XThread] {
        get {
            lockwhen(Info, _threads == null)
                _threads = loadThreads();
            _threads
        }
    }
    public Path : string { get { Info.Path } }
    public this(board : Board, number : Int64) {
        Info = board;
        _number = number;
    }
    public static MakePostId(boardPath : String, number : Int64) : string {
        $"$boardPath/$number"
    }
    public static MakePostId(boardPath : String, number : string) : string {
        $"$boardPath/$number"
    }
    loadThreads() : ConcurrentDictionary[string, XThread] {
        using(db = RepCtor()) {
            def query = db.Posts.Where(p => p.Board == Info.Path);
            def posts = db.LoadWithAttaches(query).ToList();
            def threads = ConcurrentDictionary();
            def opPosts = posts.Where(p => p.OpPostId == p.Id);
            foreach(op in opPosts) {
                def trPosts = posts.Where(p => p.OpPostId == op.Id);
                def thread = XThread(Info, trPosts);
                _ = threads.AddOrUpdate(thread.Id, thread, _ => thread);
            }
            threads
        }
    }
    public MakeAttach(imagesPath : string, f : Nancy.HttpFile) : Attach {
        def id     = Guid.NewGuid();
        def ext    = f.Name |> Path.GetExtension;
        def name   = $"$id$ext";
        def file   = Path.Combine(imagesPath, name);
        using(writer = FileStream(file, FileMode.Create))
            f.Value.CopyTo(writer);
        Attach() <- { Data = name; Id = id; Type = 0; };  
    }
    public MakePost(post : Post, attaches: Seq[Attach], opPostId : string = null) : Post {
        def getParentId(opPostId) {
            using(db = RepCtor()) 
                if(db.ExistPost(post.OpPostId, post.ParentId)) post.ParentId else opPostId
        }
        def number = GetNextNumber();
        def id     = MakePostId(Info.Path, number);
        def oId    = if(opPostId |> string.IsNullOrEmpty) id else opPostId;
        def pId    = getParentId(oId);
        _ = post <- {
            Author   = "Yoba";
            Number   = number;
            Board    = Info.Path;
            Created  = DateTime.Now;
            Id       = MakePostId(Info.Path, number);
            OpPostId = oId;
            ParentId = pId;
        };
        unless(post.Body |> string.IsNullOrEmpty) {
            def postAst  = Parser.ParsePost(post.Body, _number);
            def postHtml = Render.Render(postAst);
            post.HtmlBody = postHtml;
        }
        when(post.Id == post.OpPostId)
            post.Sage = false;
        post.Attaches = attaches.ToList();
        post
    }
    public NewThread(post : Post) : XThread {
        when(Threads != null) 
            using(db = RepCtor()) {
                db.AddPost(post);
                db.Save()
            }
        def thread  = XThread(Info, [post]);
        Threads.AddOrUpdate(thread.Id, thread, _ => thread);
    }
    public GetNextNumber() : Int64 {
        Interlocked.Increment(ref _number);
    }
    public static TryGet(key : String) : option[XBoard] {
        mutable board;
        if(!Boards.TryGetValue(key, out board)) None()
        else board |> Some
    }
    public TryGetThread(id : string) : option[XThread] {
        mutable thread;
        if(!Threads.TryGetValue(id, out thread)) None()
        else thread |> Some
    }
    static LoadBoards() : ConcurrentDictionary[string, XBoard] {
        using(db = RepCtor()) 
            db.GetBoardAndNumbers()
              .Select((b, n) => KeyValuePair(b.Path, XBoard(b, n)))
            |> ConcurrentDictionary
    }
}
