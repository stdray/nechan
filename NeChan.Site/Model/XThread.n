﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using LinqToDB;
using NBoxx.Core.Sugar;
using NeChan.Model;
using NeChan.Site.Utils;
type LQ[T] = LimitedConcurrentQueue[T];

public class XThread {
    public mutable static RepCtor : void -> IBoardRepository;
    public OpPost          : Post;
    public Board           : Board;
    syncObject             : Object = Object();
    public  PagesCache     : ConcurrentDictionary[string, FileCacheItem] = ConcurrentDictionary();
    mutable _postsCount    : int;
    mutable _attachesCount : int;
    public this(board : Board, posts : Seq[Post]) {
        Board          = board;
        OpPost         = posts.First(p => p.OpPostId == p.Id);
        _expandeds     = mkExpandeds(board.ExpandedPosts, posts);
        _updated       = posts
            .Where(p => !p.Sage || p.Id==OpPost.Id)
            .Max(p => p.Created) 
          |> Some;
        _postsCount    = posts.Count();
        _attachesCount = posts.Sum(p => p.Attaches.Count());
    }
    public Id : String { get { OpPost.Id } }
    mutable _expandeds : LQ[Post];
    Expandeds          : LQ[Post] {
        get {
            lockwhen(syncObject, _expandeds == null)
                _expandeds = GetExpandedPost();
            _expandeds
        }
    }
    public PostsTotal      : int { get { _postsCount; } }
    public PostsMissing    : int { get { _postsCount - Expandeds.Count() - 1; } }
    public AttachesMissing : int { 
        get { _attachesCount - Expandeds.Sum(p => p.Attaches.Count) - OpPost.Attaches.Count }
    }
    public ExpandedPosts   : Seq[Post] { get { Expandeds.OrderBy(p => p.Number) } }
    mutable _updated : option[DateTime].Some;
    public Updated : DateTime { get { _updated.Value; } }
    public GetReplies() : Seq[Post]{
        using(db = RepCtor()) {
            def query = db.Posts.Where(p => p.OpPostId == Id && p.Id != Id);
            db.LoadWithAttaches(query)
        }
    }
    public GetOrderedReplies() : Seq[Post]{
        GetReplies().OrderBy(p => p.Number);
    }
    public GetTree() : TreeNode[Post] {
        def replies = GetReplies();
        def posts   = [OpPost].Concat(replies);
        def tree    = TreeNode.Build(posts, _.Id, _.ParentId, p => p.ParentId != p.Id);
        tree;
    }
    public static TryGet(board : Board, number : Int64) : option[XThread] {
        TryGet(board, XBoard.MakePostId(board.Path, number));
    }
    public static TryGet(board : Board, id : string) : option[XThread] {
        using(db = RepCtor()) 
            TryGet(db, board, id)
    }
    public static TryGet(db : IBoardRepository, board : Board, id : string) : option[XThread] {
        def posts = db.Posts
                      .Where(p => p.Id == id || p.OpPostId == id)
                      .ToList();
        if(posts.Any()) XThread(board, posts) |> Some else None()
    }
    mkExpandeds(count : int, posts : Seq[Post]) : LQ[Post] {
        def queue = LQ(count);
        posts.Where(p => !p.IsOp).OrderBy(p => p.Number).Iter(queue.Enqueue);
        queue
    }
    GetExpandedPost() : LQ[Post] {
        using(db = RepCtor())  {
            def query = db.Posts
                .Where(p => p.Id != Id)
                .OrderByDescending(p => p.Number)
                .Take(Board.ExpandedPosts);
            def posts = db.LoadWithAttaches(query);
            mkExpandeds(Board.ExpandedPosts, posts);
        }
    }
    MakePostId(number : Int64) : String {
        XBoard.MakePostId(Board.Path, number);
    }
    public Add(post : Post) : void {
        post.OpPostId = Id;
        using(db = RepCtor()) {
            _ = db.AddPost(post);
            db.Save();
        }
        unless(post.Sage)
            _ = Interlocked.Exchange(ref _updated, post.Created |> Some);
        _ = Interlocked.Increment(ref _postsCount);
        _ = Interlocked.Add(ref _attachesCount, post.Attaches.Count());
        Expandeds.Enqueue(post);
    }
    public Remove(number : Int64) : void {
        def postId = MakePostId(number);
        using(db = RepCtor()) {
            def (post, attachesCount) = db.DeletePost(postId);
            db.Save();
            _ = Interlocked.Decrement(ref _postsCount);
            _ = Interlocked.Add(ref _attachesCount, -1 * attachesCount);
            unless(post.Created < Updated) {
                def upd = db.Posts
                    .Where(p => p.OpPostId == Id && !p.Sage)
                    .Max(p => p.Created);
                _ = Interlocked.Exchange(ref _updated, upd |> Some);
            }
        }
        when(Expandeds.Any(p => p.Id == postId)) 
            lock(syncObject)
                _expandeds = null;
    }
}
