﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Nancy.ModelBinding;

using Nancy;
using NeChan.Site.Utils.DynamicDictionaryExt;

[Record]
public class BoardPage {
    public Page    : Int32;
    public Board   : XBoard;
    public Threads : Seq[XThread];
    public Title   : string {
        get {
            def pageStr = if(Page > 0) $"/$Page" else string.Empty;
            $"/$(Board.Path)$pageStr - $(Board.Info.Name)"
        }
    }
}
public class BoardModule : BoardModuleBase {
     public this(pathProvider : IRootPathProvider) {
        base(pathProvider);
        this.Get["/"] = _ => 
            getPage(0);
        this.Get[@"/(?<page>[\d]{1,5})$"] = ctx => 
            GetField(ctx, "page") : String 
            |> Int32.Parse 
            |> getPage;
        this.Post["/"] = _ => 
            newThread();
        Before += ctx => {
            def page = (ctx.Parameters ?> "page").WithDefault("0");
            TryFromCache(ctx.Request.Method, Board.PagesCache, Board.Path + page);
        }
    }
    newThread() : Response {
         mutable tmp;
         def attaches = Request.Files.Map <| Board.MakeAttach(AttachesPath, _);
         def post     = Board.MakePost(Bind(), attaches);
         def thread   = Board.NewThread(post);
         _ = Board.PagesCache.TryRemove(Board.Path + "0", out tmp);
         Response.AsRedirect($"/$(thread.Id).flat");
    }
    getPage(page : int) : object {
        def threads = Board.Threads
            .Select(_.Value)
            .OrderByDescending(_.Updated)
            .Skip(page * Board.Info.ThreadsPerPage)
            .Take(Board.Info.ThreadsPerPage)
            .ToList();
        if(threads.Any() || page < 1) {
            def model = BoardPage(page, Board, threads);
            View["Index", model];
        }
        else 404;
    }
}