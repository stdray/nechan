﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Nancy;
using NeChan.Site.Utils;

public class HomeModule : NeChanModule {
    public this() {
        Get["/"]            = _ => {
            "Привет";
        }
        Get["/about"]       = _ => "About";
    }
}
