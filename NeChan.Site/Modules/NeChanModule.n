﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using Nancy;


public abstract class NeChanModule : NancyModule {
    public this(path : string = null) {
        base(if(path |> string.IsNullOrEmpty) path else path.TrimEnd(array['/']));
        After += ctx => {
            ctx.Response.Headers.Add("Content-Language", "ru");
            when(ctx.Response.ContentType |> string.IsNullOrEmpty
              || ctx.Response.ContentType == "text/html")
                ctx.Response.ContentType = "text/html; charset=UTF8";
        }
    }
    
}