﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;


using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nancy;
using NeChan.Site.Utils.DynamicDictionaryExt;
using System.Configuration;

public class BoardModuleBase : NeChanModule  {
    protected mutable Board : XBoard;
    protected RootProvider : IRootPathProvider;
    public this(pathProvider : IRootPathProvider) {
        this(pathProvider, null)
    }
    public this(pathProvider : IRootPathProvider, path : string) {
        base($"{board}/$path");
        RootProvider = pathProvider;
        Before += ctx => {
            def path = GetField(ctx.Parameters, "board");
            if(XBoard.TryGet(path) is Some(b)) {
                Board = b;
                (ViewBag :> DynamicDictionary).Add("Boards", XBoard.GetBoards());
                ctx.Response;
            }
            else 404
        }
    }
    static mutable _attachesPath : string;
    public AttachesPath : string {
        get{ 
            when(_attachesPath |> string.IsNullOrEmpty) {
                _attachesPath= Path.Combine(RootProvider.GetRootPath(), "Content", "Images");
                unless(Directory.Exists(_attachesPath))
                    _ = Directory.CreateDirectory(_attachesPath);
            }
            _attachesPath;
        }
    }
    static mutable _cachePath : string;
    public CachePath : string {
        get { 
            when(_cachePath |> string.IsNullOrEmpty) {
                _cachePath= Path.Combine(RootProvider.GetRootPath(), "Cache"); 
                unless(Directory.Exists(_cachePath))
                    _  = Directory.CreateDirectory(_cachePath);
            }
            _cachePath;
        }   
    }
    public CacheEnabled : bool { 
        get{ ConfigurationManager.AppSettings["cacheEnabled"] == "true"; }
    }
    protected TryFromCache(
        method : string,
        cache: ConcurrentDictionary[string, FileCacheItem],
        key : string) : Response {
        mutable item;
        if(CacheEnabled && method == "GET") {
            if(cache.TryGetValue(key, out item))
                Response.AsFile(item.File);
            else {
                After += ctx => CacheResponse(cache, key, ctx.Response);
                null
            }
        }
        else null
    }
    protected CacheResponse(cache: ConcurrentDictionary[string, FileCacheItem],
                            key : string, 
                            response : Response) : void {
        def file = Path.Combine(CachePath, $"$(Guid.NewGuid()).html");
        using(writer = FileStream(file, FileMode.Create))  
            response.Contents(writer);
        def item = FileCacheItem(file);
        _ = cache.AddOrUpdate(key, item, _ => item);
    }
}