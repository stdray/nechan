﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;

using NeChan.Site.Utils.DynamicDictionaryExt;

using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


public class ThreadModule : BoardModuleBase {
    protected mutable Thread : XThread;
    public this(pathProvider : IRootPathProvider) {
        base(pathProvider, "{thread}");
        Before += beforeRequest;
        After  += afterRequest;
        
        this.Get[".flat"]  = _ => View["Flat.cshtml", Thread];
        this.Post[".flat"] = addPost(_, $"/$(Thread.Id).flat");
        this.Get[".tree"]  = _ => View["Tree.cshtml", Thread];
        this.Post[".tree"] = addPost(_, $"/$(Thread.Id).tree");
    }
    addPost(_ : object, returnUri : string) : Response {
        def attaches = Request.Files.Map <| Board.MakeAttach(AttachesPath, _);
        def post     = Board.MakePost(Bind(), attaches, Thread.Id);
        Thread.Add(post);
        clearCache(post.Sage);
        Response.AsRedirect($"$returnUri#$(post.Number)");
    }
    #region util
    afterRequest(context : NancyContext) : void {
        def mode = Path.GetExtension(context.Request.Path);
        _ = context.Response.AddCookie("threadMode", mode);
    }
    beforeRequest(context : NancyContext) : Response {
        def trId = GetField(context.Parameters, "thread") : string;
        def id   = XBoard.MakePostId(Board.Info.Path, trId);
        if(id |> Board.TryGetThread is Some(t)) {
            Thread = t;
            TryFromCache(context.Request.Method, t.PagesCache, context.Request.Path);
        }
        else 404;
    }
    clearCache(sage : bool) : void {
        def page = 
            if(sage)
                Board.Threads
                    .Select(_.Value)
                    .OrderByDescending(_.Updated)
                    .ChunkIndex(Board.Info.ThreadsPerPage, 
                                t => t.Id == Thread.Id);
            else 0;
        mutable tmp;
        _ = Board.PagesCache.TryRemove(Board.Path + page.ToString(), out tmp); 
        Thread.PagesCache.Clear();
    }
    #endregion
}
