﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using NeChan.Model;

module TestInit {
    public Seed(db : IBoardRepository) : void {
        def cat = Category() <- {
            Id = Guid.NewGuid();
            Name = "Общее";
            //Boards = List();
        };
        db.AddCategory(cat);
        db.Save();
        def b = Board() <-  {
            ExpandedPosts = 5;
            Name = "Бред";
            PagesCount = 5;
            Path = "b";
            PostLen = 600;
            ThreadLen = 500;
            ThreadsPerPage = 20;
            CategoryId = cat.Id;
            //Category = cat;
        };
        db.AddBoard(b);
        def c = Board() <-  {
            ExpandedPosts = 5;
            Name = "Кодинг";
            PagesCount = 5;
            Path = "с";
            PostLen = 1000;
            ThreadLen = 400;
            ThreadsPerPage = 10;
            CategoryId = cat.Id;
            //Category = cat;
            //Posts = List();
        };
        db.AddBoard(c);
        def ftb = Board() <-  {
            ExpandedPosts = 5;
            Name = "Футбол";
            PagesCount = 5;
            Path = "ftb";
            PostLen = 700;
            ThreadLen = 300;
            ThreadsPerPage = 10;
            CategoryId = cat.Id;
            //Category = cat;
            //Posts = List();
        };
        db.AddBoard(ftb);
        db.Save();
    }
}
