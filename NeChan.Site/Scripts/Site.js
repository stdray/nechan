﻿function setPostLinkMode(ext) {
    if (ext == undefined)
        ext = getThreadMode();
    $(".postlink").each(function(){ this.href = this.href + ext + "#" + this.name; });
}

function getThreadMode() {
    ext = $.cookie("threadMode");
    if (ext == undefined)
        ext = ".flat";
    return ext;
}

function showPostForm(board, opPostId, parentNumber) {
    parentVal = board + '/' + parentNumber;
    form    = $("#postForm");
    opening = $("[name = 'opPostId']");
    parent = $("[name = 'parentId']");
    action = opPostId == "-1" ? "/" + board : "/" + opPostId + getThreadMode();
    $("[role='form']").attr("action", action);
    if (parent.val() == parentVal && form.is(":visible")) {
        form.hide();
    }
    else {
        opening.val(opPostId);
        parent.val(parentVal);
        $('#' + parentNumber).after(form)
        form.show();
    }
}