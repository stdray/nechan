﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Nancy;
using Nancy.Conventions;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using NeChan.Model;


public class CustomBoostrapper : DefaultNancyBootstrapper{
    protected override ConfigureConventions(conventions : NancyConventions) : void {
        base.ConfigureConventions(conventions);
        conventions.StaticContentsConventions.Add(
                StaticContentConventionBuilder.AddDirectory("Content", @"Content"));
        conventions.StaticContentsConventions.Add(
                StaticContentConventionBuilder.AddDirectory("Scripts", @"Scripts"));
    }
    protected override ApplicationStartup(c : TinyIoCContainer, _ : IPipelines) : void {
        def (create, seed) = match(ConfigurationManager.AppSettings["createIfNotExists"]) {
            | "seed"   => (true, true);
            | "empty"  => (true, false);
            | _        => (false, false);
        }
        using(rep = c.Resolve.[IBoardRepository]())
            when(create && rep.CreateIfNotExist() && seed)
                TestInit.Seed(rep);
        XThread.RepCtor = _ => c.Resolve.[IBoardRepository]();
        XBoard.RepCtor  = _ => c.Resolve.[IBoardRepository]();
    }
}

