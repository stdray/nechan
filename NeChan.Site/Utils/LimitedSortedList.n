﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace NeChan.Site.Utils {
    [Record]
    public class LimitedConcurrentQueue[T] : ConcurrentQueue[T] {
        public Capacity   : int;
        public new Enqueue(item : T) : void {
            when(base.Count + 1 > Capacity) {
                mutable tmp;
                _ = TryDequeue(out tmp);
            }
            base.Enqueue(item);
        }
    }
}