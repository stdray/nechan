﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

public module SeqExt {
    public Chunks[T](this seq : Seq[T], size : Int32) : Seq[Seq[T]] {
        mutable chunk = List();
        foreach(item in seq) {
            when(chunk.Count >= size) {
                yield chunk;
                chunk = List();
            }
            chunk.Add(item);
        }
        when(chunk.Any())
            yield chunk;
    }
    public ChunkIndex[T](this seq : IOrderedEnumerable[T], chunkSize : Int32, pred : T -> bool) : int {
        result : {
            foreach(item in seq with i)
                when(pred(item))
                    result(i / chunkSize);
            -1;
        }
    }
}

public class FileCacheItem : IDisposable {
    public File : string;
    mutable _disposed = false;
    public this(file : string) {
        this.File = file;
    }
    public Dispose() : void {
        Dispose(true)
    }
    protected override Finalize() : void {
        Dispose(false);
    }
    public Dispose(disposing : bool) : void {
        unless(_disposed) {
            try{ 
                File.Delete(File);
                _disposed = true;
                when(disposing) 
                    GC.SuppressFinalize(this);
            }
            catch {
                | _ => ();
            }
        }
    }
}

