﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using Nancy;

namespace NeChan.Site.Utils {
    public module DynamicDictionaryExt {
        public GetField[T](dynObj : DynamicDictionary, key : string) : T {
            (dynObj[key] :> Nancy.DynamicDictionaryValue).Value :> T;
        }
        public GetField[T](dynObj : object, key : string) : T {
            GetField(dynObj :> DynamicDictionary, key);
        }
        public @?>[T](dynObj : object, key : string) : option[T] {
            TryGetField(dynObj, key);
        }
        public TryGetField[T](dynObj : object, key : string) : option[T] {
            def dict = dynObj :> DynamicDictionary;
            if(key |> dict.ContainsKey)
                GetField(dict, key);
            else None();
        }
    }
}
