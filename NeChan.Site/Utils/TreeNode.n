﻿using Nemerle;
using Nemerle.Collections;

using System;
using SCG = System.Collections.Generic;
using System.Linq;

public class TreeNode[T] {
    public  Value  : T;
    private childs : SCG.List[TreeNode[T]] = SCG.List();
    public  Childs : Seq[TreeNode[T]] { get { childs } }
    public  Parent : TreeNode[T] { get; set; }
    public this(value : T) {
        Value  = value;
    }
    public AddChild(child : TreeNode[T]) : void {
        child.Parent = this;
        childs.Add(child);
    }
    public Level : int { 
        get { if(Parent == null) 0 else Parent.Level + 1}
    }
    public static Build[Key](items : Seq[T], toKey : T -> Key, 
                      toParent : T -> Key, hasParent : T -> bool) : TreeNode[T] { 
        def map = SCG.Dictionary();
        foreach(el in items)
            map.Add(toKey <| el, TreeNode <|el);
        mutable root;
        foreach(el in map.Values)
            if(hasParent <| el.Value)
                map[toParent <| el.Value].AddChild(el);
            else 
                if(root != null)
                    throw ArgumentException(
                        $"Multiple root in [$(toKey <| root.Value), $(toKey <| el.Value)]")
                else 
                    root = el;
        root;
    }
    public static Build[Key](items : Seq[T], toKey : T -> Key, toParent : T -> Key) : TreeNode[T]
    where Key : class {
        Build(items, toKey, toParent, toParent >> (_ != null))
    }
    public static Build[Key](items : Seq[T], toKey : T -> Key, toParent : T -> Key?) : TreeNode[T]
    where Key : struct {
        Build(items, toKey, toParent >> _.Value, toParent >> _.HasValue)
    }
    public AsEnumerable[U](comparer : Func[T, U] = null) : Seq[TreeNode[T]] where U : IComparable[U]{
        def sortedChilds = if(comparer == null) Childs else Childs.OrderBy(c => comparer(c.Value));
        [this].Concat(sortedChilds.SelectMany(c => c.AsEnumerable(comparer)))
    }
    public static @:(node : TreeNode[T]) : T {
        node.Value
    }
}
