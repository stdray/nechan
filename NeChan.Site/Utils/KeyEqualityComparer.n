﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace NeChan.Site {
    [Record]
    public class KeyEqualityComparer[T, U] : IEqualityComparer[T] where U : IEquatable[U] {
        toKey : T -> U;
        public Equals(x : T, y : T) : bool {
            toKey(x).Equals(toKey(y));
        }
        public GetHashCode(obj : T) : int {
            toKey(obj).GetHashCode();
        }
    }
    [Record]
    public class KeyComparer[T, U] : IComparer[T] where U : IComparable[U] {
        toKey : T -> U;
        public Compare(x : T, y : T) : int {
            toKey(x).CompareTo(toKey(y));
        }
        
    }
}
