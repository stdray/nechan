﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.ComputationExpressions;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NeChan.Site.Utils {

    public variant TaskResult[T] {
        | Cancel
        | Error { Value : Exception }
        | Ok    { Value : T }
    }

    public module MTask {
        public module Operators {
            public @>>-[T,U](m : Task[T], f: T -> Task[U]) : Task[U] {
                m.Bind(f)
            }
            public @-<<[T,U](f: T -> Task[U], m : Task[T]) : Task[U] {
                m.Bind(f)
            }
            public @>>.[T,U](m1 : Task[T], m2 : Task[U]) : Task[U] {
                m1 >>- (_ => m2)
            }
            public @>=>[A, B, C](f : A -> Task[B], g : B -> Task[C]) : A -> Task[C] {
                x => f(x) >>- g
            }
            public @<=<[A, B, C](g : B -> Task[C], f : A -> Task[B]) : A -> Task[C] {
                x => f(x) >>- g
            }
        }
        [Record]
        public class TaskBuilder {
            token       : CancellationToken;
            contOptions : TaskContinuationOptions;
            scheduler   : TaskScheduler;
            public this() {
                this(CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.Default)
            }
            public this(token : CancellationToken) {
                this(token, TaskContinuationOptions.None, TaskScheduler.Default)
            }
            public Return[T](value : T) : Task[T] {
                ReturnM(value);
            }
            public Zero() : Task[FakeVoid] {
                ReturnM(FakeVoid.Value);
            }
            public ReturnComp[T](t : Task[T]) : Task[T] {
                t
            }
            public Bind[T, U](task : Task[T], f : T -> Task[U]) : Task[U] {
                task.BindWithOptions(token, contOptions, scheduler, f)
            }
        }
        public TaskB : TaskBuilder = TaskBuilder();
        public Lift2[A, B](f : A * A -> B, a : Task[A], b : Task[A]) : Task[B] {
            a.Bind(aa => b.Bind(bb =>  ReturnM(f(aa, bb))))
        }
        public Run[T](t : void -> Task[T]) : TaskResult[T] {
            try {
                t().Result |> TaskResult.Ok
            }
            catch {
                | _ is OperationCanceledException                  => TaskResult.Cancel();
                | e is AggregateException 
                    when e.InnerException is TaskCanceledException => TaskResult.Cancel();
                | e                                                => TaskResult.Error(e);
            }
        }
        public MapWithOptions[T, U] (
            task : Task[T], 
            token : CancellationToken, 
            contOptions : TaskContinuationOptions, 
            scheduler : TaskScheduler, f : T -> U ) : Task[U] {
            task.ContinueWith(r => r.Result |> f, token, contOptions, scheduler)
        }
        public Map[T, U](this task : Task[T], f : T -> U) : Task[U] {
            task.ContinueWith(r => r.Result |> f)
        }
        public BindWithOptions[T, U] (
            this task : Task[T], 
            token : CancellationToken, 
            contOptions : TaskContinuationOptions, 
            scheduler : TaskScheduler, f : T -> Task[U]) : Task[U] {
            task.ContinueWith(r => r.Result |> f, token, contOptions, scheduler).Unwrap()
        }
        public Bind[T, U](this task : Task[T], f : T -> Task[U]) : Task[U] {
            task.ContinueWith(r => r.Result |> f).Unwrap()
        }
        public ReturnM[T](value : T) : Task[T] {
            def s = TaskCompletionSource();
            s.SetResult(value);
            s.Task
        }
    }

}
