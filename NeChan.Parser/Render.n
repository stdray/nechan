﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NeChan.Pygments;

namespace NeChan.Parser {
    public class Render {
        public static Render(tokens : list[Block]) : string {
            def safe = HttpUtility.HtmlEncode;
            def renderStyle(s, c) {
                def style = match(s) {
                    | MarkupTag.Bold      => "post-bold";
                    | MarkupTag.Italic    => "post-italic";
                    | MarkupTag.Spoiler   => "post-spoiler";
                    | MarkupTag.Strike    => "post-strike";
                    | MarkupTag.Underline => "post-underline";
                    | MarkupTag.Quote     => "post-quote"
                    | _                   => "";
                }
                $<#<span class="$style">$c</span>#>
            }
            def renderDefaultCode(code) {
                 $<#<pre><code>$(safe(code))</code></pre>#>;
            }
            def renderToken(t) {
                | Block.NewLine =>
                    "<br />";
                | Block.Link(l) => 
                    def t = if(l.Length > 100) l.Substring(0, 100) else l;
                    $<#<a href="$l">$(safe(t))</a>#>;
                | Block.PostRef(n) =>
                    $<#<a href="#$n">>>$n</a>#>;
                | Block.Text(t) => 
                    safe(t);
                | Block.List(isNum, lines) =>
                def (start, end) = if(isNum) ("<ol>", "</ol>") else ("<ul>","</ul>");
                def strLines = lines
                    .Select(Render)
                    .Select(r => "<li>" + r + "</li>");
                start + string.Join(Environment.NewLine, strLines) + end;
                | Block.Tag(s, cs) => 
                    renderStyle(s, Render(cs));
                | Block.Code(l, code) =>
                    if(l is Some(lang)) {
                        def codeHtml = CodeRender.Instanse.Render(lang.ToLowerInvariant().Trim(), code);
                        if(codeHtml != null) codeHtml;
                        else renderDefaultCode(code);
                    }
                    else renderDefaultCode(code);
            }
            string.Join(string.Empty, tokens.Select(renderToken));
        }
    }
}
