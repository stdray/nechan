﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Peg;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using NT = Nemerle.Peg.NToken;

namespace NeChan.Parser {
    [PegGrammar(Option = EmitDebugSources, post, grammar {
        D                           = ['0'..'9'];
        CH                          = [Any];
        enCH                        = ['a'..'z'];
        SP                          = [Zs] / '\t' / '\v' / '\f';
        NL                          = "\r\n" / '\n' / '\r'/ '\u2028' / '\u2029';
        ESC                         = '\\';
        escCH                       = ESC CH;
        @nl          : void         = NL;
        @s           : void         = SP;
        @=           : void         = '=';
        @+           : void         = '+';
        @-           : void         = '-';
        #region text style
        boldTxt        : Block        = (escCH / (!"**" !inner CH))+;
        bold           : Block        = "**" (inner / boldTxt)+ "**";
        italicTxt      : Block        = (escCH / (!"//" !inner CH))+;
        italic         : Block        = "//" (inner / italicTxt)+ "//";
        spoilerTxt     : Block        = (escCH / (!"%%" !inner CH))+;
        spoiler        : Block        = "%%" (inner / spoilerTxt)+ "%%";
        strikeTxt      : Block        = (escCH / (!"--" !inner CH))+;
        strike         : Block        = "--" (inner / strikeTxt)+ "--";
        underlineTxt   : Block        = (escCH / (!"__" !inner CH))+; 
        underline      : Block        = "__" (inner / underlineTxt)+ "__";
        #endregion
        #region code
        lang           : string       = enCH+;
        codeEnd        : void         = '[' @s* '/' @s* "code" @s* ']';
        code           : Block        = '[' @s* "code" @s* (@= @s* lang)? @s* ']' (!codeEnd CH)* codeEnd;
        #endregion
        #region list
        lineTxt        : Block        = (!NL !inner CH)+;
        lineBody       : List[Block]  = (inner / lineTxt)+;
        numLst         : Block        = (nl @s* @+ @s+ lineBody)+ (&NL / !CH);
        comLst         : Block        = (nl @s* @- @s+ lineBody)+ (&NL / !CH);
        lst            : Block        = (numLst / comLst);
        #endregion
        #region quote
        quoteTxt       : Block        = '>' lineTxt (&NL / !CH);
        quotePost      : Block        = ">>" D+;
        quote          : Block        = quotePost / quoteTxt ;
        #endregion
        #region link
        protocol                      = "https" / "http";
        linkStart                     = protocol "://";
        invalidChars                  = NL / SP / '(' / ')';
        link           : Block        = linkStart (!invalidChars CH)+; 
        #endregion
        newLine        : Block        = NL;
        escape         : Block        = "[[" (!"]]" CH)+ "]]";
        inner          : Block        = bold / italic / strike / spoiler/ underline 
                                      / escape / quotePost / link;
        block          : Block        = code / lst / inner / quote / newLine;
        token          : Block        = block / newLine;
        txt            : Block        = (escCH / !token CH)+;
        content        : Block        = (token / txt); 
        post           : list[Block]  = content+;
    })]
    public class Parser {
        postNumber : long;
        this(postNumber : long){
            this.postNumber = postNumber;
        }
        lang(t : NT) : string { t |> GetText }
        bold(_ : NT, c : List[Block], _ : NT) : Block { Block.Tag(MarkupTag.Bold, c.NToList()) }
        italic(_ : NT, c : List[Block], _ : NT) : Block { Block.Tag(MarkupTag.Italic, c.NToList()) }
        spoiler(_ : NT, c : List[Block], _ : NT) : Block { Block.Tag(MarkupTag.Spoiler, c.NToList()) }
        strike(_ : NT, c : List[Block], _ : NT) : Block { Block.Tag(MarkupTag.Strike, c.NToList()) }
        underline(_ : NT, c : List[Block], _ : NT) : Block { Block.Tag(MarkupTag.Underline, c.NToList()) }
        escape(_ : NT, c : NT, _ : NT) : Block { c |> GetText |> Block.Text }
        code(_ : NT, _ : NT, l : option[string], _ : NT, c : NT) : Block { Block.Code(l, c |> GetText) }
        boldTxt(t : NT) : Block { txt(t) }
        italicTxt(t : NT) : Block { txt(t) }
        spoilerTxt(t : NT) : Block { txt(t) }
        strikeTxt(t : NT) : Block { txt(t) }
        underlineTxt(t : NT) : Block { txt(t) }
        lineTxt(t : NT) : Block { txt(t) }
        newLine(_ : NT) : Block { Block.NewLine() }
        quoteTxt(_ : NT, q : Block) : Block { Block.Tag(MarkupTag.Quote, [q]) }
        quotePost(t : NT, d : NT) : Block { 
            def numStr =  d |> GetText;
            mutable num;
            if(!long.TryParse(numStr, out num) || num > postNumber)
                GetText(t) + numStr |> Block.Text;
            else 
                Block.PostRef(num);
        }
        txt(t : NT) : Block { 
            def tt = t |> GetText;
            tt |> Block.Text; 
        }
        numLst(ls : List[List[Block]]) : Block { Block.List(true, ls.Map(_.NToList())) }
        comLst(ls : List[List[Block]]) : Block { Block.List(false, ls.Map(_.NToList())) }
        post(cs : List[Block]) : list[Block] { cs.NToList(); }
        link(p : NT, t : NT) : Block { 
            GetText(p) + GetText(t) |> Block.Link 
        }
        
        public static ParsePost(str : string, num : long = 0) : list[Block] {
            def s = SourceSnapshot(str).WithText(str.ToLower());
            def p = Parser(num);
            if(p.Parse(s) is Some(bs)) bs
            else throw ArgumentException("Incorrect string");
        }
    }
    public enum MarkupTag {
        | Bold
        | Italic
        | Spoiler
        | Strike
        | Underline
        | Quote
    }
    public variant Block {
        | NewLine;
        | Tag  { tag  : MarkupTag; content : list[Block] }
        | Code { lang : option[string]; code : string }
        | Text { text : string; }
        | List { numbered : bool; content : list[list[Block]] }
        | PostRef { number : long }
        | Link { uri : string;}
    }
}
