﻿//using LinqToDB.Mapping;

using System;
using System.Collections.Generic;
using LinqToDB.Mapping;

namespace NeChan.Model {
    public class Category {
        //[Key]
        public virtual Id             : Guid        { get; set; }
        //[StringLength(140), Required]
        public virtual Name           : string      { get; set; }
        //public virtual Boards         : Coll[Board] { get; set; }
    }
    
    public class Board {
        //[Key, StringLength(10)]
        public virtual Path           : string       { get; set; }
        public virtual PagesCount     : int          { get; set; }
        public virtual PostLen        : int          { get; set; }
        public virtual ThreadLen      : int          { get; set; }
        public virtual ExpandedPosts  : int          { get; set; }
        public virtual ThreadsPerPage : int          { get; set; }
        //[StringLength(140), Required]
        public virtual Name           : string       { get; set; }
        //public virtual Posts          : Seq[Post]   { get; set; }
        public virtual CategoryId     : Guid         { get; set; }
        //public virtual Category       : Category     { get; set; }
    }

    public class Post {
        //[Key, StringLength(30)]
        public virtual Id            : string       { get; set; }
        //[Required]
        public virtual Author        : string       { get; set; }
        //[StringLength(140)]
        public virtual Title         : string       { get; set; }
        public virtual Body          : string       { get; set; }
        public virtual HtmlBody      : string       { get; set; }
        public virtual Created       : DateTime     { get; set; }
        //[StringLength(30), Required]
        public virtual ParentId      : string       { get; set; }
        //[StringLength(30), Required]
        public virtual OpPostId      : string       { get; set; }
        //[StringLength(10), Required]
        public virtual Board         : string       { get; set; }
        public virtual Number        : long         { get; set; }
        public virtual Sage          : bool         { get; set; }
        public virtual IsDeleted     : bool         { get; set; }
        [NotColumn]
        public virtual Attaches      : ICollection[Attach] { get; set; }
        [NotColumn]
        public virtual IsOp          : bool         { get { Id == OpPostId } }
    }

    public class Attach {
        //[Key]
        public virtual  Id   : Guid       { get; set; }
        public virtual Type  : int        { get; set; }
        //[StringLength(700)]
        public virtual Data  : string     { get; set; }
        //public virtual Posts : Coll[Post] { get; set; }
    }
}
