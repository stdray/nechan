﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace NeChan.Model {
    public interface IBoardRepository : IDisposable {
        AddBoard(board : Board) : void;
        AddCategory(category : Category) : void;
        Boards : IQueryable[Board] { get; }
        GetBoardAndNumbers() : Seq[Board * long];
        Posts : IQueryable[Post]  { get; }
        LoadWithAttaches(postsQuery : IQueryable[Post]) : Seq[Post];
        AddPost(post : Post) : void;
        DeletePost(postId : string) : Post * int;
        ExistPost(opPostId : string, postId : string) : bool;
        CreateIfNotExist() : bool;
        Save() : void;
    }
}
