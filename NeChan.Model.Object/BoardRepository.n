﻿using NeChan.Model;
using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Linq;
using NBoxx.Core.Sugar;
using NDatabase;
using NDatabase.Api;

namespace NeChan.Model.Object {
    public class BoardRepository : IBoardRepository {
        ConStrName = "NeChan";
        conStr : string;
        lockObj : object = object;
        mutable _db : IOdb;
        public this() {
            def str = ConfigurationManager.ConnectionStrings["NeChan"];
            when(str == null)
                throw ArgumentException($"Connection string $ConStrName not fount");
            conStr = str.ConnectionString;
        }
        db : IOdb { 
            get {
                lockwhen(lockObj, _db == null)
                    _db = OdbFactory.Open(conStr);
                _db
            } 
        }
        public AddPost(post : Post) : void {
            _ = Boards.First(b => b.Path == post.Board);
            _ = db.Store(post);
        }
        public Boards : IQueryable[Board] { 
            get { db.AsQueryable(); } 
        }
        public DeletePost(postId : string) : (Post * int) {
            def post = db.AsQueryable.[Post]().First(p => p.Id == postId);
            post.IsDeleted = true;
            (post, post.Attaches.Count)
        }
        public ExistPost(opPostId : string, postId : string) : bool {
            Posts.Any(p => p.OpPostId == opPostId && p.Id == postId)
        }
        public GetBoardAndNumbers() : IEnumerable[(Board * long)]{ 
            foreach(b in Boards.ToList()) {
                def ps = Posts.Where(p => p.Board == b.Path);
                def n = if(ps.Any()) ps.Max(p => p.Number) else 0L;
                yield (b, n);
            }
        }
        public LoadWithAttaches(postsQuery : IQueryable[Post]) : Seq[Post] {
            postsQuery.ToList();
        }
        public Posts : IQueryable[Post] { 
            get { db.AsQueryable() } 
        }
        public CreateIfNotExist() : bool {
            !System.IO.File.Exists(conStr);
        }
        public Save() : void { 
            db.Commit();
        }
        public AddBoard(board : Board) : void {
            _ = db.AsQueryable.[Category]().First(c => c.Id == board.CategoryId);
            _ = db.Store(board);
        }
        public AddCategory(category : NeChan.Model.Category) : void {
            _ = db.Store(category);
        }
        public Dispose() : void { 
            lockwhen(lockObj, _db != null) {
                _db.Dispose();
                _db = null;
            }
        }
    }
}
