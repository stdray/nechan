﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using LinqToDB;

namespace NeChan.Model.SqlServer {
    public partial class NeChanDB : NeChan.Model.IBoardRepository {
        IQueryable<NeChan.Model.Board> NeChan.Model.IBoardRepository.Boards {
            get { return Boards; }
        }
        IQueryable<NeChan.Model.Post> NeChan.Model.IBoardRepository.Posts {
            get { return  Posts; }
        }
        public IEnumerable<NeChan.Model.Post> LoadWithAttaches(IQueryable<NeChan.Model.Post> query) {
            var postsAttaches = from p in query
                                from pa in PostAttaches.Where(x => x.PostId == p.Id).DefaultIfEmpty()
                                select new {
                                    Post = p,
                                    Attach = pa != null ? pa.Attach : null
                                };
            var dict = new Dictionary<string, NeChan.Model.Post>();
            foreach (var pa in postsAttaches.ToList()) {
                if (!dict.ContainsKey(pa.Post.Id)) {
                    pa.Post.Attaches = new List<Model.Attach>();
                    if (pa.Attach != null)
                        pa.Post.Attaches.Add(pa.Attach);
                    dict[pa.Post.Id] = pa.Post;
                }
                else if (pa.Attach != null)
                    dict[pa.Post.Id].Attaches.Add(pa.Attach);
            }
            return dict.Values;
        }
        public void AddPost(NeChan.Model.Post post) {
            this.Insert(post);
            if (post.Attaches != null) {
                foreach (var attach in post.Attaches) {
                    this.Insert(attach);
                    this.Insert(new PostAttach {
                        PostId = post.Id,
                        AttachId = attach.Id
                    });
                }
            }
        }
        public Nemerle.Builtins.Tuple<Model.Post, int> DeletePost(string postId) {
            Posts.Where(p => p.Id == postId)
                 .Set(p => p.IsDeleted, true)
                 .Update();
            var pi = Posts
                .Where(p => p.Id == postId)
                .Select(p => new { Post = p, AttachesCount = p.PostAttaches.Count() })
                .First();
            return new Nemerle.Builtins.Tuple<Model.Post, int>(null, pi.AttachesCount);
        }
        public bool ExistPost(string opPostId, string Id) {
            return Posts.Any(p => p.OpPostId == opPostId && p.Id == Id);
        }
        public IEnumerable<Nemerle.Builtins.Tuple<Model.Board, long>> GetBoardAndNumbers() {
            var query = from b in Boards
                        let n = b.Posts.Any() ? b.Posts.Max(p => p.Number) : 0L
                        select new { b, n };
            foreach (var bn in query)
                yield return new Nemerle.Builtins.Tuple<Model.Board, long>(bn.b, bn.n);
        }
        public void AddBoard(Model.Board board) {
            this.Insert(board);
        }
        public void AddCategory(Model.Category category) {
            this.Insert(category);
        }
        public bool CreateIfNotExist() {
            return !Categories.Any();
        }
        public void Save() {
        }
    }

    public sealed partial class Category : NeChan.Model.Category { }
    public sealed partial class Post : NeChan.Model.Post { }
    public sealed partial class Attach : NeChan.Model.Attach { }
    public sealed partial class Board : NeChan.Model.Board { }
}
