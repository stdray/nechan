﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

using System.Reflection;
using IronPython.Hosting;
using Ionic.Zip;


namespace NeChan.Pygments {
    public class CodeRender : IDisposable {
        private const string PythonLibArc = "IronPythonLib.zip";
        private readonly string LibPath;
        private static readonly CodeRender instanse = new CodeRender();
        public static CodeRender Instanse { get { return instanse; } }
        private readonly dynamic formater;
        private readonly Func<string, dynamic> getLexerF;
        private readonly Func<string, dynamic, dynamic, string> highlightF;
        private readonly ConcurrentDictionary<string, dynamic> lexerCache = new ConcurrentDictionary<string, dynamic>();
        public readonly string Status;
        public readonly Exception Error;
        public static string ExttractLibs() {
            var tmpDir  = Path.Combine(Path.GetTempPath(), "NechanIronPythonLib");
            var appPath = AppDomain.CurrentDomain.BaseDirectory;
            string[] paths = { Path.Combine(appPath, "bin", PythonLibArc),
                               Path.Combine(appPath, "AppData", PythonLibArc) };
            var libPath = paths.First(File.Exists);
            using (var zip = ZipFile.Read(libPath)) 
                zip.ExtractAll(tmpDir, ExtractExistingFileAction.OverwriteSilently);
            return tmpDir;
        }
        private CodeRender() {
            try {
                LibPath          = ExttractLibs();
                var python       = Python.CreateEngine();
                var paths        = python.GetSearchPaths();
                paths.Add(Path.Combine(LibPath, "Lib"));
                python.SetSearchPaths(paths);
                var pygments     = python.ImportModule("pygments.lexers");
                pygments.ImportModule("pygments.formatters");
                dynamic engine   = (dynamic)pygments;
                formater         = engine.formatters.HtmlFormatter();
                getLexerF        = engine.lexers.get_lexer_by_name;
                highlightF       = engine.highlight;
                Status           = "Ok";
            }
            catch (Exception ex) {
                Status = ex.Message;
                Error = ex;
            }
        }
        private dynamic GetLexer(string name) {
            dynamic lexer;
            if(lexerCache.TryGetValue(name, out lexer)){
                return lexer;
            }
            lexer = getLexerF(name);
            if(lexer==null) 
                return null;
            return lexerCache.GetOrAdd(name, lexer);
        }
        public string Render(string lang, string code) {
            if (Error != null)
                return null;
            var lexer = GetLexer(lang);
            if (lexer == null)
                return null;
            var rendered = highlightF(" " + code, lexer, formater);
            return rendered;
        }

        public void Dispose(bool disposing) {
            if (Directory.Exists(LibPath))
                Directory.Delete(LibPath);
            if (!Directory.Exists(LibPath) && disposing) 
                GC.SuppressFinalize(this);
        }
        public void Dispose() {
            Dispose(true);
        }
        ~CodeRender() {
            Dispose(false);
        }
    }
}
