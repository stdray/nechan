﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeChan.Parser;

namespace NeChan.Tests {
    [TestClass]
    public class ParserTest {
        static Tags : list[MarkupTag] =  [
                MarkupTag.Bold, MarkupTag.Italic, MarkupTag.Spoiler,
                MarkupTag.Strike, MarkupTag.Underline
        ];
        [TestMethod]
        public Text() : void {
            def str = "asdfgzxxcv";
            def r = match(Parser.ParsePost(str)) {
                | [Block.Text(s)] => s == str;
                | _               => false;
            }
            Assert.IsTrue(r);
        }
        static StrTag(tag : MarkupTag) : string {
            | MarkupTag.Bold => "**";
            | MarkupTag.Italic => "//";
            | MarkupTag.Strike => "--";
            | MarkupTag.Underline => "__";
            | MarkupTag.Spoiler => "%%";
            | MarkupTag.Quote  => ">";
        }
        static MkText(tag : MarkupTag, str : string) : string {
            def stag = StrTag(tag);
            $"$stag$str$stag"
        }
        [TestMethod]
        public SimpleBlock() : void {
            def testStr = "dafsdfsdfdsf";
            def testTag(tag, str) {
                def post = MkText(tag, str);
                def r = Parser.ParsePost(post);
                def eq = match(r) {
                    | [Block.Tag(tag2, [Block.Text(str2)])] => 
                        tag2 == tag && str2 == str;
                    | _                    => false;
                }
                Assert.IsTrue(eq, tag.ToString());
            }
            foreach(t in Tags)
                testTag(t, testStr);
        }
        [TestMethod]
        public EscapeInBlockSimple() : void {
            def (testStr) = ("daf", "sdfdsf");
            def testTag(tag, (s1, s2)) {
                def str  = s1 + @"\" + StrTag(tag) + s2;
                def post = MkText(tag, str);
                def r = Parser.ParsePost(post);
                def eq = match(r) {
                    | [Block.Tag(tag2, [Block.Text(str2)])] => 
                        tag2 == tag && str2 == str;
                    | _                    => false;
                }
                Assert.IsTrue(eq, tag.ToString());
            }
            foreach(t in Tags)
                testTag(t, testStr);
        }
        [TestMethod]
        public CodeBlock() : void {
            def lang = "nemerle";
            def code  = "dsfsdfsfdsdfdsf";
            def str = $"[code=$lang]$code[/code]";
            def r = Parser.ParsePost(str);
            System.Diagnostics.Trace.WriteLine(Render.Render(r));
            def eq = match(r) {
                | [ Block.Code(Some(l), c) ] => lang == l && code == c;
                | _                    => false;
            }
            Assert.IsTrue(eq, "Language specified");
            def str = $"[code]$code[/code]";
            def r = Parser.ParsePost(str);
            def eq = match(r) {
                | [ Block.Code(None(), c) ] => code == c;
                | _                    => false;
            }
            Assert.IsTrue(eq, "Language not specified");
        }
        [TestMethod]
        public EscapeBlock() : void {
            def txt = MkText(MarkupTag.Bold, "asdfdsfsdfsdf");
            def post = $"[[$txt]]";
            def r = match(Parser.ParsePost(post)) {
                | [ Block.Text(t)] => t == txt;
                | _ => false;
            }
            Assert.IsTrue(r);
        }
        [TestMethod]
        public BlockSequence() : void {
            def (s1, s2, s3) = ("asd", "sad", "l;k");
            def (t1, t2)     = (MarkupTag.Bold, MarkupTag.Strike);
            def (p1, p2)     = (MkText(t1, s1), MkText(t2, s2));
            def post = $"$p1 sdfsdf $p2$s3";
            def r = match(Parser.ParsePost(post)) {
                | [Block.Tag(tt1, [Block.Text(m1)]),
                   Block.Text, 
                   Block.Tag(tt2, [Block.Text(m2)]),
                   Block.Text(ss3)] => tt1 == t1 && m1 == s1 && tt2 == t2 && 
                                       m2 ==s2 && ss3 == s3;
                | _ => false;
            }
            Assert.IsTrue(r);
        }
        [TestMethod]
        public InnerBlocks() : void {
            def (s1, s2, s3) = ("asd", "sad", "l;k");
            def (t1, t2)     = (MarkupTag.Bold, MarkupTag.Italic);
            def (p1, p2)     = (MkText(t1, s1), MkText(t2, s2));
            def post = $"abasd %%$p1 $p2%%$s3";
            def parsed = Parser.ParsePost(post);
            def r = match(parsed) {
                | [Block.Text, 
                   Block.Tag(MarkupTag.Spoiler, [
                        Block.Tag(tt1, [Block.Text(ss1)]),
                        Block.Text,
                        Block.Tag(tt2, [Block.Text(ss2)])]),
                   Block.Text(ss3)] => tt1 == t1 && ss1 == s1 && tt2 == t2 &&
                                       ss2 == s2 && ss3 == s3;
                                      
                | _ => false;
            }
            Assert.IsTrue(r);
        }
        [TestMethod]
        public ListBlock() : void {
            def lst = <#
            - a
            - b 
            - c 
            #>;
            def parsed = Parser.ParsePost(lst);
            Assert.IsTrue(parsed is [
                Block.List(false, [[Block.Text],[Block.Text],[Block.Text]]),
                Block.NewLine,
                Block.Text], "Unnumbered");
            
            def lst2 = <#__uuuu__
            + a
            + **b** sdfdsfsdfsdf [code=ruby]zxc[/code]
            + c 
            #>;
            def parsed2 = Parser.ParsePost(lst2);
            Assert.IsTrue(parsed2 is [
                Block.Tag(MarkupTag.Underline, _),
                Block.List(true, [[Block.Text],[Block.Tag, Block.Text],[Block.Text]]),
                Block.NewLine,
                Block.Text], "Numbered");
        }
         [TestMethod]
        public Quote() : void {
            def lst = <# >skadljasldkjasdlk#>;
            def parsed = Parser.ParsePost(lst);
            Assert.IsTrue(parsed is [Block.Text, Block.Tag(MarkupTag.Quote, _)]);
        }
        [TestMethod]
        public PostRef() : void {
            def lst = <# >>2222 #>;
            def parsed = Parser.ParsePost(lst, 9999);
            Assert.IsTrue(parsed is [Block.Text, Block.PostRef(2222), Block.Text]);
        }
        [TestMethod]
        public Link() : void {
            def lst = <# https://github.com/typesafehub/co #>;
            def parsed = Parser.ParsePost(lst);
            Assert.IsTrue(parsed is [Block.Text, Block.Link, Block.Text]);
        }
    }
    
}
